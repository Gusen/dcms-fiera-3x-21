<?

include_once '../sys/inc/start.php';
include_once '../sys/inc/compress.php';
include_once '../sys/inc/sess.php';
include_once '../sys/inc/home.php';
include_once '../sys/inc/settings.php';
include_once '../sys/inc/db_connect.php';
include_once '../sys/inc/ipua.php';
include_once '../sys/inc/fnc.php';
include_once '../sys/inc/adm_check.php';
include_once '../sys/inc/user.php';
include_once '../sys/inc/icons.php';
user_access('adm_menu',null,'/');
adm_check();
$set['title'] = 'Главное меню';
include_once H.'sys/inc/thead.php';
title();

$opendiricon=opendir(H.'style/icons');
while ($icons=readdir($opendiricon))
{
	// запись всех тем в массив
	if (preg_match('#^\.|default.png#',$icons))continue;
	$icon[] = $icons;
}closedir($opendiricon);


if (isset($_POST['add']) && isset($_POST['name']) && $_POST['name']!=NULL)
{

$name = my_esc($_POST['name']);
$url = my_esc($_POST['url']);
$counter = my_esc($_POST['counter']);
$type = my_esc($_POST['type']);
$pos = mysql_result(query("SELECT MAX(`pos`) FROM `menu`"), 0)+1;
$icon = preg_replace('#[^a-z0-9 _\-\.]#i', null, $_POST['icon']);
query("INSERT INTO `menu` (`name`, `url`, `counter`, `pos`, `icon`, `type`) VALUES ('$name', '$url', '$counter', '$pos', '$icon', '$type')");

$_SESSION['message'] = lang('Ссылка успешно добавлена');
exit(header('Location: ?'));
}

if (isset($_POST['change']) && isset($_GET['id']) && isset($_POST['name']) && $_POST['name']!=NULL && isset($_POST['url']) )
{

$id = abs((int)$_GET['id']);
$name = my_esc($_POST['name']);
$url = my_esc($_POST['url']);
$counter = isset($_POST['counter']) ? my_esc($_POST['counter']) : null;
$icon = preg_replace('#[^a-z0-9 _\-\.]#i', null, $_POST['icon']);

query("UPDATE `menu` SET `name` = '$name', `url` = '$url', `counter` = '$counter', `icon` = '$icon' WHERE `id` = '$id' LIMIT 1");

$_SESSION['message'] = lang('Пункт меню успешно изменен');
exit(header('Location: ?'));
}

if (isset($_GET['id']) && isset($_GET['act']) && mysql_result(query("SELECT COUNT(*) FROM `menu` WHERE `id` = '".intval($_GET['id'])."'"),0))
{
$menu = mysql_fetch_assoc(query("SELECT * FROM `menu` WHERE `id` = '".intval($_GET['id'])."' LIMIT 1"));

if ($_GET['act']=='up')
{
	query("UPDATE `menu` SET `pos` = '".($menu['pos'])."' WHERE `pos` = '".($menu['pos']-1)."' LIMIT 1");
	query("UPDATE `menu` SET `pos` = '".($menu['pos']-1)."' WHERE `id` = '".intval($_GET['id'])."' LIMIT 1");
	$_SESSION['message'] = lang('Пункт меню сдвинут на позицию вверх');
	exit(header('Location: ?'));
}

if ($_GET['act']=='down')
{
	query("UPDATE `menu` SET `pos` = '".($menu['pos'])."' WHERE `pos` = '".($menu['pos']+1)."' LIMIT 1");
	query("UPDATE `menu` SET `pos` = '".($menu['pos']+1)."' WHERE `id` = '".intval($_GET['id'])."' LIMIT 1");
	$_SESSION['message'] = lang('Пункт меню сдвинут на позицию вниз');
	exit(header('Location: ?'));
}

if ($_GET['act']=='del')
{
	query("DELETE FROM `menu` WHERE `id` = '".intval($_GET['id'])."' LIMIT 1");
	query("UPDATE `menu` SET `pos` = ( SELECT @var := @var +1 FROM (SELECT @var :=0) AS tbl ) ;");
	$_SESSION['message'] = lang('Пункт меню удален');
	exit(header('Location: ?'));
}

}


err();
aut();
echo "<table class='post'>";

$q = mysql_query("SELECT * FROM `menu` ORDER BY `pos` ASC");
while ($post = mysql_fetch_assoc($q))
{
echo "<tr>";

if (!isset($post['icon']))mysql_query('ALTER TABLE `menu` ADD `icon` VARCHAR( 32 ) NULL DEFAULT NULL');
if (!isset($post['type']))mysql_query("ALTER TABLE  `menu` ADD  `type` ENUM('link', 'razd', 'inc') NOT NULL DEFAULT 'link' AFTER `id`");

echo "<td class='p_t'>";
if ($post['type']=='link')echo icons($post['icon'],'code');
echo output_text($post['pos']) ." ". output_text($post['name']) ." ".($post['type'] == 'link' ? "(". output_text($post['url']) : null);
echo "</td>";
echo "</tr>";
echo "<tr>";
echo "<td class='p_m'>";

if (isset($_GET['id']) && $_GET['id']==$post['id'] && isset($_GET['act']) && $_GET['act']=='edit')
{

echo "<form action='?id=$post[id]' method='post'>";

if ($post['type'] != 'inc')
echo "Тип: ".($post['type'] == 'link' ? 'Ссылка':'Разделитель')."<br />";
echo "Название <br /><input type='text' name='name' value='$post[name]' /><br />";

if ($post['type']=='link' or $post['type']=='inc')
{
	echo "Ссылка:<br />";
	echo "<input type='text' name='url' value='$post[url]' /><br />";
}
else echo "<input type='hidden' name='url' value='' />";

if ($post['type']!='inc')
{
	echo "Счетчик:<br />";
	echo "<input type='text' name='counter' value='$post[counter]' /><br />";
}

if ($post['type']=='link')
{
echo "Иконка:<br />";
echo "<select name='icon'>";
echo "<option value='default.png'>По умолчанию</option>";
for ($i=0;$i<sizeof($icon);$i++)
{
	echo "<option value='$icon[$i]'".($post['icon'] == $icon[$i]?" selected='selected'":null).">$icon[$i]</option>";
}
echo "</select><br />";
}
else echo "<input type='hidden' name='icon' value='$post[icon]' />";
echo "<input class='submit' name='change' type='submit' value='Изменить' /><br />";
echo "</form>";
echo "<a href='?'>Отмена</a><br />";
}
else
{
if ($post['type']!='inc')
echo "Счетчик: ".($post['counter'] == null ? 'отсутствует': output_text($post['counter']))."<br />";
else
echo "Виджет: ".output_text($post['url'])."<br />";
echo "<a href='?id=$post[id]&amp;act=up'>Выше</a> | ";
echo "<a href='?id=$post[id]&amp;act=down'>Ниже</a> | ";
echo "<a href='?id=$post[id]&amp;act=del'>Удалить </a><br />";
echo "<a href='?id=$post[id]&amp;act=edit'>Редактировать </a><br />";
}

echo "</td>";
echo "</tr>";
}
echo "</table>";

if (isset($_GET['add'])){
echo "<div class='p_m'><form action='?add' method='post'>";
echo "Тип:<br />";
echo "<select name='type'>";
echo "<option value='link'>Ссылка (1)</option>";
echo "<option value='razd'>Раздел (2)</option>";
echo "<option value='inc'>Виджет (3)</option>";
echo "</select><br />";
echo "Название (1,2):<br />";
echo "<input type='text' name='name' value=\"\"/><br />";
echo "Ссылка(1):<br />";
echo "<input type='text' name='url' value=''/><br />";
echo "Счетчик (1,2):<br />\n";
echo "<input type='text' name='counter' value=''/><br />";
echo "Иконка (1):<br />";
echo "<select name='icon'>";
echo "<option value='default.png'>По умолчанию</option>";
for ($i=0;$i<sizeof($icon);$i++)
{
	echo "<option value='$icon[$i]'>$icon[$i]</option>";
}
echo "</select><br />";
echo "<input class='submit' name='add' type='submit' value='Добавить' /><br />";
echo "<a href='?'>Отмена</a><br />";
echo "</form></div>";
}
else echo "<div class='foot'><a href='?add'>Добавить пункт</a></div>";

echo "<div class='foot'>";
echo "&laquo;<a href='".APANEL."'>В админку</a><br />";
echo "</div>";

include_once H.'sys/inc/tfoot.php';
?>